//
//  AbstractPredicateBuilder.h
//
//  Created by A-25 on 24/12/15.
//  Copyright © 2015 rentateam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AbstractPredicateBuilder : NSObject

@property(nonatomic, strong) NSMutableArray *params;
@property(nonatomic, strong) NSMutableArray *conditions;

-(NSPredicate*)buildPredicate;
-(void)resetBeforeCreate;

@end
