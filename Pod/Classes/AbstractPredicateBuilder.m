//
//  AbstractPredicateBuilder.m
//
//  Created by A-25 on 24/12/15.
//  Copyright © 2015 rentateam. All rights reserved.
//

#import "AbstractPredicateBuilder.h"

@implementation AbstractPredicateBuilder

-(id)init
{
    self = [super init];
    if(self != nil){
        self.params = [NSMutableArray new];
        self.conditions = [NSMutableArray new];
    }
    return self;
}

-(void)resetBeforeCreate
{
    [self.params removeAllObjects];
    [self.conditions removeAllObjects];
}

-(NSPredicate*)buildPredicate
{
    NSMutableArray *sqlParts = [NSMutableArray new];
    for(unsigned int i = 0; i < [self.conditions count]; i++){
        [sqlParts addObject:self.conditions[i]];
    }
    if([sqlParts count] == 0){
        return nil;
    }

    NSPredicate *predicate = [NSPredicate predicateWithFormat:[sqlParts componentsJoinedByString:@" AND "] argumentArray:self.params];
    return predicate;
}

@end
