# RTPredicateBuilder

[![CI Status](http://img.shields.io/travis/a-25/RTPredicateBuilder.svg?style=flat)](https://travis-ci.org/a-25/RTPredicateBuilder)
[![Version](https://img.shields.io/cocoapods/v/RTPredicateBuilder.svg?style=flat)](http://cocoapods.org/pods/RTPredicateBuilder)
[![License](https://img.shields.io/cocoapods/l/RTPredicateBuilder.svg?style=flat)](http://cocoapods.org/pods/RTPredicateBuilder)
[![Platform](https://img.shields.io/cocoapods/p/RTPredicateBuilder.svg?style=flat)](http://cocoapods.org/pods/RTPredicateBuilder)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

RTPredicateBuilder is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "RTPredicateBuilder"
```

## Author

a-25, anuryadov@gmail.com

## License

RTPredicateBuilder is available under the MIT license. See the LICENSE file for more info.
